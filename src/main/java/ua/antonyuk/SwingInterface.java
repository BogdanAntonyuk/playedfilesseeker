package ua.antonyuk;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class SwingInterface {
    SQLiteAccessor sqLiteAccessor = new SQLiteAccessor("open conn");

    JFrame frame = new JFrame("FileSeeker");
    private JTextField filePathText = new JTextField(sqLiteAccessor.lastFilePathText());
    private JTextField dateText = new JTextField(sqLiteAccessor.lastDateText());
    private JTextField fileNameText = new JTextField(sqLiteAccessor.lastfileNameText());


    public void Main() {

        frame.setSize(700, 400);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setLocationRelativeTo(null);
        frame.setLayout(new GridBagLayout());

        JLabel dateLabel = new JLabel("Enter date \'yyymmdd\'");
        JLabel fileNameLabel = new JLabel("Enter File Name");
        JLabel filePathLabel = new JLabel("Enter file path here");

        JButton finishButton = new JButton("ok");
        finishButton.addActionListener(new MyButton());


        frame.add(filePathLabel, new GridBagConstraints(0, 0, 1, 1, 1, 1,
                GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL, new Insets(1, 1, 1, 20), 0, 0));
        frame.add(dateLabel, new GridBagConstraints(1, 0, 1, 1, 1, 1,
                GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL, new Insets(1, 1, 1, 20), 0, 0));
        frame.add(fileNameLabel, new GridBagConstraints(2, 0, 1, 1, 1, 1,
                GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL, new Insets(1, 1, 1, 1), 0, 0));
        frame.add(filePathText, new GridBagConstraints(0, 1, 1, 1, 1, 1,
                GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL, new Insets(1, 1, 1, 20), 0, 0));
        frame.add(dateText, new GridBagConstraints(1, 1, 1, 1, 1, 1,
                GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL, new Insets(1, 1, 1, 20), 0, 0));
        frame.add(fileNameText, new GridBagConstraints(2, 1, 1, 1, 1, 1,
                GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL, new Insets(1, 1, 1, 1), 0, 0));
        frame.add(finishButton, new GridBagConstraints(0, 3, 3, 1, 1, 1,
                GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, new Insets(1, 1, 1, 1), 0, 0));


        frame.setVisible(true);
        frame.pack();
        sqLiteAccessor.close();


    }

    class MyButton implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            SQLiteAccessor sqLiteAccessor = new SQLiteAccessor();
            sqLiteAccessor.connect();
            XMLParser parser = new XMLParser();
            parser.searchXMLFile(filePathText.getText(), dateText.getText());
            parser.xmlParser(fileNameText.getText());
            sqLiteAccessor.insert(filePathText.getText(),dateText.getText(),fileNameText.getText());
            sqLiteAccessor.close();



        }
    }


}

