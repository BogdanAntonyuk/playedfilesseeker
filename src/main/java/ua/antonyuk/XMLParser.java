package ua.antonyuk;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.swing.*;
import javax.swing.event.DocumentListener;
import javax.swing.event.UndoableEditListener;
import javax.swing.text.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class XMLParser {



    String matchingFile;
    protected String searchXMLFile(String filePath, String  date){
        File f = null;
        if( new File(filePath).isDirectory()){
            f = new File(filePath);
        }else{
            System.out.println("There is no such directory");
            System.exit(1);
        }

        File[] matchingFiles = f.listFiles((dir, name) -> name.contains(date));
        return this.matchingFile = String.valueOf(matchingFiles[0]);
    }


    protected void xmlParser(String searchWord){
        JFrame frame2=new JFrame("Results");
        frame2.setSize(400, 400);
        frame2.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame2.setLayout(new FlowLayout(FlowLayout.LEADING));
        JTextArea result = new JTextArea(40,40);







        try {



            File xmlFile = new File(matchingFile);
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            Document doc = db.parse(xmlFile);
            Element root = doc.getDocumentElement();
            NodeList nl = root.getChildNodes();
            int count = 0;
            for (int i = 0; i < nl.getLength(); i++) {
                Node node = nl.item(i);
                if (node.getNodeType() == Node.ELEMENT_NODE) {
                    Element element = (Element) node;
                    if ("Play".equals(node.getNodeName())) {
                        String fileName = element.getElementsByTagName("FileName").item(0).getChildNodes().item(0).getNodeValue();
                        if (fileName.toLowerCase().contains(searchWord.toLowerCase())){
                            count++;
                            System.out.println("Time code: " + element.getElementsByTagName("TimeCode").item(0).getChildNodes().item(0).getNodeValue());
                            System.out.println("File Name: " + fileName);
                            System.out.println("----------------------");
                            result.append("Time code: " + element.getElementsByTagName("TimeCode").item(0).getChildNodes().item(0).getNodeValue()+"\n");
                            result.append("File Name: " + fileName+"\n");
                            result.append("----------------------"+"\n");


                        }
                    }
                }
            }
            if (count==0){
                System.out.println("nothing was found");
                result.append("nothing was found");
            }
            System.out.println("Number of occurrences: "+count);


//            Pattern pattern = Pattern.compile("\\d{8}");
//            Matcher matcher = pattern.matcher(String.valueOf(xmlFile));
//            System.out.println("File path is: "+ String.valueOf(xmlFile));
//            matcher.find();
//            String sDate = matcher.group(0);
//            System.out.println(sDate);
//            DateFormat format = new SimpleDateFormat("yyyyMMdd");
//            Date date = format.parse(sDate);
//            System.out.println(date);

        } catch (ParserConfigurationException | SAXException | IOException e) {
            e.printStackTrace();
        }
        result.setCaretPosition(0);


        JScrollPane scrollPane = new JScrollPane(result);
        frame2.add(scrollPane, BorderLayout.CENTER);


        frame2.setVisible(true);
        frame2.pack();

    }

}
