package ua.antonyuk;

import java.sql.*;

public class SQLiteAccessor {
    Connection conn;
    Statement statement;
    ResultSet rs;

    public SQLiteAccessor(String openConnectionMarker) {
        try {
            // db parameters
            String url = "jdbc:sqlite:D:\\MyJavaProjects\\PlayedFileReport\\src\\Files\\lastInput.db";
            // create a connection to the database
            conn = DriverManager.getConnection(url);

            System.out.println("Connection to SQLite has been established.");
            System.out.println();

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }

    }

    public SQLiteAccessor() {
    }

    public void connect() {
        try {
            // db parameters
            String url = "jdbc:sqlite:D:\\MyJavaProjects\\PlayedFileReport\\src\\Files\\lastInput.db";
            // create a connection to the database
            conn = DriverManager.getConnection(url);

            System.out.println("Connection to SQLite has been established.");
            System.out.println();

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
    public void close(){
        try {
            if (conn != null) {
                conn.close();
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }

    }
    public void insert(String filePathText, String dateText, String fileNameText){
        String querryDelete = "DELETE FROM inputs ";
        String querryInsert = "INSERT INTO inputs (filePath, date, fileName) "+
                "VALUES ('"+filePathText+"', '"+dateText+"', '"+fileNameText+"')";
        try {
            statement = conn.createStatement();
            statement.executeUpdate(querryDelete);
            statement.executeUpdate(querryInsert);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public String lastFilePathText() {
        String querrySelect = "SELECT filePath FROM inputs";
        String lastFilePathText = null;
        try {
            statement = conn.createStatement();
            rs = statement.executeQuery(querrySelect);
            rs.next();
            lastFilePathText = rs.getString("filePath");
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return lastFilePathText;

    }
    public String lastDateText() {
        String querrySelect = "SELECT date FROM inputs";
        String lastDateText = null;
        try {
            statement = conn.createStatement();
            rs = statement.executeQuery(querrySelect);
            rs.next();
            lastDateText = rs.getString("date");
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return lastDateText;

    }
    public String lastfileNameText() {
        String querrySelect = "SELECT fileName FROM inputs";
        String lastfileNameText = null;
        try {
            statement = conn.createStatement();
            rs = statement.executeQuery(querrySelect);
            rs.next();
            lastfileNameText = rs.getString("fileName");
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return lastfileNameText;

    }
}
